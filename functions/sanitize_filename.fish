# Replace whitespace and special characters

function sanitize_filename -a original_filename -d "Renames file without unwanted characters and formatting"

  if test -z $original_filename
    echo Argument is empty.
    return 1
  end

  if ! test -e $original_filename
    echo cannot find file: $original_filename
    return 1
  end

  set -l modified_filename (
    echo $original_filename |
    sed 's/-.\{4,11\}\.m4a/\.m4a/g' | # removes youtube-dl suffix
    tr ' ' '_' | # remove whitespace
    tr '[:upper:]' '[:lower:]' | # forces lowercase
    tr '\+' '_' | # remove plus_signs
    tr '\'' '' | # remove single-quote
    sed 's/\._/_/g' | # removes ugly periods
    )
  echo "'$original_filename' => '$modified_filename'"
  mv $original_filename $modified_filename
end
