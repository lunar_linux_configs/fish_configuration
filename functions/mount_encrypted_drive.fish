# Mounts a specified LUKS encrypted drive
#

# ORIGINAL
# sudo cryptsetup luksOpen /dev/sdc sdc && sudo mount /dev/mapper/sdc $HOME/nas_backup_local

function mount_encrypted_drive -d "Mount a LUKS encrypted drive through cryptsetup"
   set -l DRIVE (lsblk | grep -o 'sd\w' | sort | uniq | fzf)

   sudo cryptsetup luksOpen /dev/$DRIVE $DRIVE && \
     sudo mount /dev/mapper/$DRIVE $HOME/nas_backup_local && \
     printf "\nDrive $DRIVE Mounted Successfully!\n" || printf "\nDrive $DRIVE Mount Failed...\n"

   echo "$DRIVE -> $HOME/nas_backup_local" >> ~/.mounted_drives
end

