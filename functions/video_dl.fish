function video_dl -d "Downloads video with the given URL" -a URL FILENAME

  printf "Downloading Audio File\n"

  yt-dlp -f 140 -o "audio.m4a" $URL

  printf "Downloading Video File\n"

  yt-dlp -f 399 -o "video.mp4" $URL

  printf "MUXING files File\n"

  ffmpeg -i video.mp4 -i audio.m4a -c copy $FILENAME

  rm video.mp4 audio.m4a

  echo "Final outout has finished! @ ->  [$FILENAME]"
end

