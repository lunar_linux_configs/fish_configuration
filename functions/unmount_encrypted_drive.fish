# Unmounts a specified LUKS encrypted drive
#

function unmount_encrypted_drive -d "Unmount a LUKS encrypted drive through cryptsetup"
   set -l DRIVE (cat ~/.mounted_drives | fzf | cut -d ' ' -f 1)

   sudo umount $HOME/nas_backup_local && \
     sudo cryptsetup luksClose $DRIVE && \
     printf "\nDrive $DRIVE Unmounted Successfully!\n" || printf "\nDrive $DRIVE Unmount Failed...\n"

   sed --in-place "/$DRIVE/d" $HOME/.mounted_drives
end

