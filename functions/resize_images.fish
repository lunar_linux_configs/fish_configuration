# resize all large .jpg && .png images to standard size

function resize_images -a filename_head

  if test -z $filename_head
    echo no argument
    mogrify -resize 2000X2000\> *.jpg

    ls *.png; and begin
      mogrify -resize 2000X2000\> -format jpg *.png
      rm *.png
    end
  else
    echo $filename_head
    mogrify -resize 2000X2000\> $filename_head*.jpg

    ls $filename_head*.png; and begin
      mogrify -resize 2000X2000\> -format jpg $filename_head*.png
      rm $filename_head*.png
    end

  end

end

