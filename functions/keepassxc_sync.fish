# Remotely pulls and merges the keepassxc database from another machine

function keepassxc_sync
  set -l SYNC_HOST (list_hosts | fzf)

  if test -z $SYNC_HOST
    echo no argument
    return 1
  else
    scp $SYNC_HOST:'~/Documents/.security/securepass.kdbx' $HOME/Documents/.security/securepass_$SYNC_HOST.kdbx

    if test -e $HOME/Documents/.security/securepass_$SYNC_HOST.kdbx
      keepassxc-cli merge -s $HOME/Documents/.security/securepass.kdbx $HOME/Documents/.security/securepass_$SYNC_HOST.kdbx
    else
      printf "\nkeepass.kdbx from $SYNC_HOST was not found locally\n\n"
      return 1
    end

    if test $status -eq 0
      printf "\nkeepass.kdbx has been merged with $SYNC_HOST\n\n"

      rm $HOME/Documents/.security/securepass_$SYNC_HOST.kdbx
      scp $HOME/Documents/.security/securepass.kdbx $SYNC_HOST:'~/Documents/.security/securepass.kdbx'
      printf "\nkeepass.kdbx has back-synced with $SYNC_HOST\n\n"
    else
      printf "\nkeepass.kdbx failed to merge files from $SYNC_HOST\n\n"
      return 1
    end
  end
end

